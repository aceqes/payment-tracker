package com.bsc.homework.service;

import com.bsc.homework.component.ConversionCalculator;
import com.bsc.homework.component.TrackingFileReader;
import com.bsc.homework.component.TrackingValidator;
import com.bsc.homework.config.BeanFactory;
import com.bsc.homework.dto.BalanceDto;
import com.bsc.homework.dto.FileProcessingResult;
import com.bsc.homework.dto.PaymentEntryDto;
import com.bsc.homework.store.TrackingStore;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Main business logic service
 */
public class TrackingService {

    private final TrackingStore trackingStore;
    private final TrackingValidator trackingValidator;
    private final TrackingFileReader trackingFileReader;
    private final ConversionCalculator conversionCalculator;

    public TrackingService() {
        trackingStore = BeanFactory.getTrackingStore();
        trackingValidator = BeanFactory.getTrackingValidator();
        trackingFileReader = BeanFactory.getTrackingFileReader();
        conversionCalculator = BeanFactory.getConversionCalculator();
    }

    /**
     * Processes payment input (from cmd or file) and adds it to store
     * @param input payment input in format of CURRENCY SUM
     */
    public void processInput(String input) {
        PaymentEntryDto paymentEntry = trackingValidator.validatePaymentInput(input);
        trackingStore.addPayment(paymentEntry);
    }

    /**
     * @return Current balance map with currency as key. Values contain object with currency,
     * value and optionally convertedValue (if conversion rate is available)
     */
    public Map<String, BalanceDto> getCurrentBalances() {
        Map<String, BigDecimal> balances = trackingStore.getCurrentBalances();
        return convertBalancesToDtos(balances);
    }

    /**
     * Processes payment inputs from file.
     * @param fileName file to read data from
     * @return object containing import statistics (total, success, error)
     */
    public FileProcessingResult processFile(String fileName) {
        List<String> inputs = trackingFileReader.readFileEntries(fileName);
        int success=0, errors=0;
        for (String input : inputs) {
            try {
                processInput(input);
                success++;
            } catch (IllegalArgumentException e) {
                // do not log errors in processing for now, just count them
                errors++;
            }
        }
        return new FileProcessingResult(inputs.size(), success, errors);
    }

    private Map<String, BalanceDto> convertBalancesToDtos(Map<String, BigDecimal> balances) {
        Map<String, BalanceDto> balanceDtos = new HashMap<>();
        for (Map.Entry<String, BigDecimal> e : balances.entrySet()) {
            String currency = e.getKey();
            BigDecimal value = e.getValue();

            BalanceDto balanceDto = new BalanceDto();
            balanceDto.setCurrency(currency);
            balanceDto.setValue(value);
            balanceDto.setConvertedValue(conversionCalculator.convert(value, currency));

            balanceDtos.put(currency, balanceDto);
        }
        return balanceDtos;
    }

}
