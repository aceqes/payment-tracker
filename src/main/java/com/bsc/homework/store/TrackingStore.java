package com.bsc.homework.store;

import com.bsc.homework.dto.PaymentEntryDto;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TrackingStore {

    /**
     * we only need to store current balance values, so let's use simple map
     * with key=currency && value=balance
     *
     * In case we needed to store all the payments, we'd use map
     * with key=currency and value=LinkedList of money values (LinkedList because of order)
     */
    private final Map<String, BigDecimal> balanceHolderMap;

    public TrackingStore() {
        // ConcurrentHashMap because we expect simultaneous writes and reads to/from store
        balanceHolderMap = new ConcurrentHashMap<>();
    }

    /**
     * Adds payment to balance map
     * If there's no such currency yet, add it as new entry in balanceHolderMap
     * Otherwise calculate new value by summing current and added value.
     * In case result is zero, then remove key (=currency) from map altogether.
     *
     * @param paymentEntry
     */
    public void addPayment(PaymentEntryDto paymentEntry) {
        String currency = paymentEntry.getCurrency();
        BigDecimal newBalance = calculateNewBalance(paymentEntry);
        if (newBalance.compareTo(BigDecimal.ZERO) != 0){
            balanceHolderMap.put(currency, newBalance);
        } else {
            balanceHolderMap.remove(currency);
        }
    }

    /**
     * @return current balance map with currency as key and numerical (BigDecimal) value of current balance
     */
    public Map<String, BigDecimal> getCurrentBalances() {
        return balanceHolderMap;
    }

    private BigDecimal calculateNewBalance(PaymentEntryDto paymentEntry) {
        String currency = paymentEntry.getCurrency();
        BigDecimal currentBalance = balanceHolderMap.get(currency);

        BigDecimal newBalance;
        if (currentBalance == null) {
            newBalance = paymentEntry.getAmount();
        } else {
            newBalance = currentBalance.add(paymentEntry.getAmount());
        }
        return newBalance;
    }

}
