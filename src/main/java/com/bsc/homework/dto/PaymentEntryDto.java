package com.bsc.homework.dto;

import java.math.BigDecimal;

public class PaymentEntryDto {

    private String currency;
    private BigDecimal amount;

    public PaymentEntryDto(String currency, BigDecimal amount) {
        this.setCurrency(currency);
        this.setAmount(amount);
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
