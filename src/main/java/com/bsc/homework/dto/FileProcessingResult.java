package com.bsc.homework.dto;

public class FileProcessingResult {

    private int total;
    private int success;
    private int error;

    public FileProcessingResult(int total, int success, int error) {
        this.total = total;
        this.success = success;
        this.error = error;
    }

    public int getTotal() {
        return total;
    }

    public int getSuccess() {
        return success;
    }

    public int getError() {
        return error;
    }

}
