package com.bsc.homework.config;

import com.bsc.homework.component.ConversionCalculator;
import com.bsc.homework.component.TrackingFileReader;
import com.bsc.homework.component.TrackingValidator;
import com.bsc.homework.controller.TrackingController;
import com.bsc.homework.service.TrackingService;
import com.bsc.homework.store.TrackingStore;
import com.bsc.homework.view.TrackingView;

/**
 * Singletones beans factory
 *
 * This would normally be handled by Spring or other CDI framework,
 * but to keep it simple & framework-free I implemented own factory / beans holder
 */
public class BeanFactory {

    private static TrackingController trackingController;
    private static TrackingService trackingService;
    private static TrackingView trackingView;
    private static TrackingStore trackingStore;
    private static TrackingValidator trackingValidator;
    private static TrackingFileReader trackingFileReader;
    private static ConversionCalculator conversionCalculator;

    public static TrackingController getTrackingController() {
        if (trackingController == null) {
            trackingController = new TrackingController();
        }
        return trackingController;
    }

    public static TrackingService getTrackingService() {
        if (trackingService == null) {
            trackingService = new TrackingService();
        }
        return trackingService;
    }

    public static TrackingView getTrackingView() {
        if (trackingView == null) {
            trackingView = new TrackingView();
        }
        return trackingView;
    }

    public static TrackingStore getTrackingStore() {
        if (trackingStore == null) {
            trackingStore = new TrackingStore();
        }
        return trackingStore;
    }

    public static TrackingValidator getTrackingValidator() {
        if (trackingValidator == null) {
            trackingValidator = new TrackingValidator();
        }
        return trackingValidator;
    }

    public static TrackingFileReader getTrackingFileReader() {
        if (trackingFileReader == null) {
            trackingFileReader = new TrackingFileReader();
        }
        return trackingFileReader;
    }

    public static ConversionCalculator getConversionCalculator() {
        if (conversionCalculator == null) {
            conversionCalculator = new ConversionCalculator();
        }
        return conversionCalculator;
    }
}
