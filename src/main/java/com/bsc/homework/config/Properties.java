package com.bsc.homework.config;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Properties {

    // application constants
    public static final long TIMER_INITIAL_DELAY = 60 * 1000L;
    public static final long TIMER_PERIOD = 60 * 1000L;
    public static final int INPUT_MIN_LENGTH = 5; // currency + space + one digit
    public static final int INPUT_PARTS = 2; // currency + number
    public static final int CURRENCY_LENGTH = 3;
    public static final int AMOUNT_MAX_SCALE = 2;

    // exit command
    public static String EXIT_COMMAND = "exit";

    // texts
    public static String WELCOME_MESSAGE = System.lineSeparator() + "*** Welcome to BSC Payment Tracker (assignment created by Stanislav Repko) ***"
            + System.lineSeparator();

    public static String PLEASE_ENTER_MESSAGE = System.lineSeparator() + "Please enter payment in format CURRENCY SUM, e.g. \"USD 100\" or \"exit\" to finish:";
    public static String PAYMENT_ADDED_MESSAGE = "Payment added successfully";

    public static String WRONG_COMMAND_MESSAGE = "Wrong command entered.";

    public static final String INPUT_STRING_INVALID = "Payment entry must be in correct format: CURRENCY SUM, e.g. USD 100";
    public static final String CURRENCY_INVALID = "Currency must be 3-letter string, e.g. USD";
    public static final String AMOUNT_INVALID = "Amount must be valid monetary value. Please use digits, minus sign and a dot as decimal separator";

    public static final String CURRENT_BALANCE_MESSAGE = System.lineSeparator() + "Current balance status:";
    public static final String NO_BALANCES_MESSAGE = "No balances at the moment!";

    public static final String CANNOT_READ_FILE = "File cannot be read. Either it does not exist or is not properly formatted";
    public static final String PROCESSED_FILE_MESSAGE = "Processed source file with %d items (%d succesfully, %d errors)";

    // conversion rates
    public static final String CONVERSION_CURRENCY = "USD";
    public static final Map<String, BigDecimal> CONVERSION_RATES = new HashMap<>();
    static {
        CONVERSION_RATES.put("EUR", new BigDecimal("1.13"));
        CONVERSION_RATES.put("JPY", new BigDecimal("0.0093"));
    }

}
