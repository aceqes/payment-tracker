package com.bsc.homework.component;

import com.bsc.homework.dto.PaymentEntryDto;

import java.math.BigDecimal;

import static com.bsc.homework.config.Properties.*;

public class TrackingValidator {

    /**
     * Validates payment input. Checks for:
     * 1. input string is not empty
     * 2. input string consists of exactly two parts separated by space
     * 3. first part contains valid currency - any 3-letter string
     * 4. second part contains valid amount
     *
     * @param input payment input string
     * @return PaymentEntryDto containing currency and amount
     */
    public PaymentEntryDto validatePaymentInput(String input) {
        validateInputString(input);

        String[] parts = input.split(" ");
        validateInputParts(parts);

        String currency = validateCurrency(parts[0]);
        BigDecimal amount = validateAmount(parts[1]);

        // create dto
        return new PaymentEntryDto(currency, amount);
    }

    private void validateInputString(String input) {
        validate(input != null && input.trim().length() >= INPUT_MIN_LENGTH,
                INPUT_STRING_INVALID);
    }

    private void validateInputParts(String[] parts) {
        validate(parts != null && parts.length == INPUT_PARTS,
                INPUT_STRING_INVALID);
    }

    /**
     * ANY 3-letter string
     */
     private String validateCurrency(String currency) {
        validate(currency != null && currency.trim().length() == CURRENCY_LENGTH,
                CURRENCY_INVALID);
        return currency.trim().toUpperCase();
    }

    /**
     * ASSUMPTION:
     * For now let's assume payment values can only be entered
     * in format parseable by BigDecimal string constructor
     *
     * In reality, there would be plenty of possible formats depending on country, language (locale), etc.
     * @return
     */
    private BigDecimal validateAmount(String amount) {
        BigDecimal bdValue = null;
        try {
            bdValue = new BigDecimal(amount);
        } catch (NumberFormatException nfe) {
           // ignore exception now because this check is for validation purposes only;
           // normally we would at least log the warning
        }
        validate(bdValue != null && bdValue.scale() <= AMOUNT_MAX_SCALE,
                AMOUNT_INVALID);
        return bdValue;
    }

    private void validate(boolean condition, String message) {
        if (!condition) {
            throw new IllegalArgumentException(message);
        }
    }

}
