package com.bsc.homework.component;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.bsc.homework.config.Properties.AMOUNT_MAX_SCALE;
import static com.bsc.homework.config.Properties.CONVERSION_RATES;

public class ConversionCalculator {

    public BigDecimal convert(BigDecimal value, String currency) {
        if (value != null && CONVERSION_RATES.containsKey(currency)) {
            BigDecimal conversionRate = CONVERSION_RATES.get(currency);
            return value.multiply(conversionRate).setScale(AMOUNT_MAX_SCALE, RoundingMode.HALF_UP);
        }
        return null;
    }

}
