package com.bsc.homework.component;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.bsc.homework.config.Properties.CANNOT_READ_FILE;

public class TrackingFileReader {

    public List<String> readFileEntries(String fileName) {
        List<String> lines = new ArrayList<>();
        File file = new File(fileName);
        // try with resources -> automatically closes resources after finish / on exception
        try (BufferedReader br = new BufferedReader(new java.io.FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(CANNOT_READ_FILE, e);
        }
        return lines;
    }

}
