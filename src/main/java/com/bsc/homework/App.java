package com.bsc.homework;

import com.bsc.homework.config.BeanFactory;
import com.bsc.homework.controller.TrackingController;

/**
 * BSC Payment Tracker assignment
 */
public class App 
{

    private static TrackingController trackingController;

    public static void main(String[] args)
    {
        trackingController = BeanFactory.getTrackingController();
        trackingController.start(args);
    }

}
