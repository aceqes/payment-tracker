package com.bsc.homework.view;

import com.bsc.homework.dto.BalanceDto;
import com.bsc.homework.dto.FileProcessingResult;

import java.util.Map;

import static com.bsc.homework.config.Properties.*;

public class TrackingView {

    /**
     * Displays welcome message
     */
    public void welcomeMessage() {
        print(WELCOME_MESSAGE);
    }

    /**
     * Displays "enter payment" prompt
     */
    public void enterPaymentMessage() {
        print(PLEASE_ENTER_MESSAGE);
    }

    /**
     * Displays errors from exception
     */
    public void showError(Exception e) {
        printError(WRONG_COMMAND_MESSAGE + System.lineSeparator() + e.getLocalizedMessage());
    }

    /**
     * Displays current balance list
     */
    public void currentBalanceMessage(Map<String, BalanceDto> currentBalances) {
        print(CURRENT_BALANCE_MESSAGE);
        if (currentBalances.isEmpty()) {
            print(NO_BALANCES_MESSAGE);
        } else {
            for (Map.Entry<String, BalanceDto> balance : currentBalances.entrySet()) {
                BalanceDto balanceDto = balance.getValue();
                String balanceMessage = createBalanceMessage(balanceDto);
                print(balanceMessage);
            }
        }
        print(System.lineSeparator());
    }

    /**
     * Displays file processing statisics
     */
    public void showFileProcessingResult(FileProcessingResult res) {
        print(String.format(PROCESSED_FILE_MESSAGE, res.getTotal(), res.getSuccess(), res.getError()));
    }

    /**
     * Displays confirmation after successfully added payment
     */
    public void paymentAddedMessage() {
        print(PAYMENT_ADDED_MESSAGE);
    }

    private void print(String text) {
        System.out.println(text);
    }

    private void printError(String text) {
        System.err.println(text);
    }

    private String createBalanceMessage(BalanceDto balanceDto) {
        StringBuilder sb = new StringBuilder(balanceDto.getCurrency()).append(": ").append(balanceDto.getValue());
        if (balanceDto.getConvertedValue() != null) {
            sb.append(" (").append(CONVERSION_CURRENCY).append(": ").append(balanceDto.getConvertedValue()).append(")");
        }
        return sb.toString();
    }

}
