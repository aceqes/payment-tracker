package com.bsc.homework.controller;

import com.bsc.homework.config.BeanFactory;
import com.bsc.homework.dto.BalanceDto;
import com.bsc.homework.dto.FileProcessingResult;
import com.bsc.homework.service.TrackingService;
import com.bsc.homework.view.TrackingView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.bsc.homework.config.Properties.*;

/**
 * Main input-output (controller) class
 */
public class TrackingController {

    private final TrackingService trackingService;
    private final TrackingView trackingView;
    private final BufferedReader inputStream;
    private final Timer timer;

    public TrackingController() {
        trackingService = BeanFactory.getTrackingService();
        trackingView = BeanFactory.getTrackingView();
        inputStream = new BufferedReader(new InputStreamReader(System.in));
        timer = new Timer();
    }

    /**
     * Starts input-output terminal to insert payment inputs. Starts timer to display current balances
     *
     * @param cmdArguments array of command line arguments
     */
    public void start(String[] cmdArguments) {
        trackingView.welcomeMessage();
        processArguments(cmdArguments);
        startCurrentBalancesTimer();
        readInputs();
        stopCurrentBalancesTimer();
    }

    private void processArguments(String[] cmdArguments) {
        if (cmdArguments != null && cmdArguments.length == 1) {
            FileProcessingResult fileProcessingResult = trackingService.processFile(cmdArguments[0]);
            trackingView.showFileProcessingResult(fileProcessingResult);
        }
    }

    private void readInputs() {
        String input = null;
        do {
            try {
                trackingView.enterPaymentMessage();
                input = inputStream.readLine();
                if (EXIT_COMMAND.equals(input)) return;
                trackingService.processInput(input);
                trackingView.paymentAddedMessage();
            } catch (IOException | IllegalArgumentException e) {
                trackingView.showError(e);
            }
        } while (!EXIT_COMMAND.equals(input));
    }

    private void startCurrentBalancesTimer() {
        TimerTask repeatedTask = new TimerTask() {
            public void run() {
                Map<String, BalanceDto> currentBalances = trackingService.getCurrentBalances();
                trackingView.currentBalanceMessage(currentBalances);
            }
        };
        timer.scheduleAtFixedRate(repeatedTask, TIMER_INITIAL_DELAY, TIMER_PERIOD);
    }

    private void stopCurrentBalancesTimer() {
        timer.cancel();
    }

 }
