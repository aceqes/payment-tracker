###BSC homework (created by Stanislav Repko)

After checkout, application can be simply built and run by:
```
mvn package
java -cp target/paymenttracker-1.0-SNAPSHOT.jar com.bsc.homework.App data.txt
```
where last argument - _data.txt_ - is optional filename with input payments to process

#####I implemented:
- base task
- file import
- conversion rates

#####I did not implement:
- unit tests (although structure is prepared and I'll be more than happy to add it, if you want)

#####Assumptions / design choices:
- since no framework/CDI mechanism was used, components for the application are created and provided by simple BeanFactory
- application uses MVC design, so the main entry point for all commands and inputs is TrackingController class
- inputs are processed by TrackingService, all outputs are served by TrackingView class (which uses System.out and System.err
for now, but this can be easily replaced with anything else in the future and we'll only need to replace it in one place)
- TrackingStore is DAO object which for now just stores the results in ConcurrentHashMap in memory; again this
can be easily replaced by actual storage mechanism / DB engine
- we assume format of the number can be directly parsed by BigDecimal's string constructor. In reality, that might not be the case,
as different regions/languages will want to input numbers in their own format which will need to be parsed
- all properties/constants are contained (hardcoded) in Properties.java class for simplicity; in real-world application it would be
e.g. in application.properties file (and/or language bundles) and easily modifiable without the need for recompilation after changes